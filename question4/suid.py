

if __name__ == '__main__':
  import os
  # get euid
  euid = os.geteuid()
  # get EGID
  egid = os.getegid()

  print("euid: {}".format(euid))
  print("egid: {}".format(egid))
