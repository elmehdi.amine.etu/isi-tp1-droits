#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    FILE *f;
    uid_t uid = getuid();
    uid_t euid = geteuid();
    gid_t gid = getgid();
    gid_t egid = getegid();
    printf("UID : %u \nEUID : %u \nGID : %u \nEGID : %u\n", uid, euid, gid, egid);

    f = fopen("mydir/data.txt", "r");
    if (f == NULL)
    {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    printf("File opens correctly\n");
    // show the file content
    char c;
    while ((c = fgetc(f)) != EOF)
    {
        printf("%c", c);
    }
    fclose(f);
    exit(EXIT_SUCCESS);

    return 0;
}
