# creer le groupe groupe_a
groupadd groupe_a
# creer le groupe groupe_b
groupadd groupe_b

# avec un utilisateur a de groupe_a on cree un repertoire dir_a
mkdir dir_a

# avec un utilisateur b de groupe_b on cree un repertoire dir_b
mkdir dir_b
chmod g- dir_b

# l'utilisateur admin cree un repertoire dir_c
mkdir dir_c
chmod +t dir_c
chmod ag-w dir_c
