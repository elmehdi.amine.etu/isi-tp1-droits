# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- AMINE, EL-Mehdi, elmehdi.amine.etu@univ-lille.fr

## Question 1

Le processus peut bien ecrire dans le fichier parce que l'utilisateur appartient au groupe qui a le droit d'ecrire dans le fichier.

## Question 2

- Le caractere x dans les droits d'acces d'un repertoire signifie qu'on ne peut pas ni executer ni consulter les fichiers dans ce repertoire.

- L'utilisateur toto ne peut pas acceder le repertoire mydir car il n'y a pas de droit d'execution pour le groupe ou toto appartient.

- apres avoir execute la commande `ls -al mydir`, on a bien eu une liste des fichiers mais sans informations sur leur droits d'acces, l'utilisateur ou groupe de ce fichier.

## Question 3

- La valeur des ids sont 1001, le processus n'arrive pas à ouvrir le fichier mydir/data.txt en lecture.

- Apres avoir activé le flag set-user-id du fichier executable, la valeur d EUID a changé a 1002 et le processus a pu ouvrir le fichier mydir/data.txt en lecture.

## Question 4

- La valeur des ids est 1001.

- changer le droit d'execution d'utilisateur au flag `set-user-id`.

## Question 5

- La commande `chfn`sert à changer le nom et les informations de l'utilisateur.

```bash
  > ls -al /usr/bin/chfn
  -rwsr-xr-x 1 root root 85064 juil.  15  2021 /usr/bin/chfn
```

Le fichier chfn est executable par tous les utilisateurs, il est modifiable que par l'utilisateur `root`, lisible par tous les utilisaturs, il est surtout executable avec des privileges superieurs à l'utilisateur `root`.

## Question 6

Les mots de passe sont stockés au fichier `/etc/shadow`, #TODO Pourquoi ?

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests.
